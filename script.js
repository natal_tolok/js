var sum = 0;
console.log("Squares from 10 to 20:");
for (var i = 10; i<= 20; i++) {
	console.log(i*i);
	sum += i;
}

console.log("Sum from 10 to 20:\n" + sum);



function buttonClick() {
	var x1 = document.getElementById('x1').value;
	var x2 = document.getElementById('x2').value;
	if ( x1 == "" || x2 == "" ) {
		alert("Поля x1 и x2 должны быть заполнены.");
	}
	var x1 = +x1;
	var x2 = +x2;
	var resultDiv = document.getElementById('result');
	if (Number.isNaN(x1) || Number.isNaN(x2)) {
		alert("В поля x1 и x2 должны быть введены числовые значения.");
	} else {
		var radios = document.getElementsByName('action');
		if (radios[0].checked)
		{
			var res = 0;
			for (var i = x1; i<= x2; i++) {
				res += i;
			}
		} else {
			var res = 1;
			for (var i = x1; i<= x2; i++) {
				res *= i;
			}

		}
		resultDiv.innerHTML = res;
	}
}

function clearClick() {
	var x1Div = document.getElementById('x1');
	var x2Div = document.getElementById('x2');
	x1Div.value = "";
	x2Div.value = "";
}